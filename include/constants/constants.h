/*******************************************************************************************************************************
 * include/constants/constants.h
 ******************************************************************************************************************************/

#ifndef CFX_CONSTANTS_CONSTANTS_H_
#define CFX_CONSTANTS_CONSTANTS_H_

namespace cfx::constants {

// Project constants
inline const char ProjectName[] = {"C++ Microservice Example"};

} // namespace cfx::constants

#endif // CFX_CONSTANTS_CONSTANTS_H_

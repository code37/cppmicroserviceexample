/*******************************************************************************************************************************
 * include/handlers/get_handler.h
 ******************************************************************************************************************************/

#ifndef CFX_HANDLERS_GET_HANDLER_H_
#define CFX_HANDLERS_GET_HANDLER_H_

#include "controller/handler_map.h"
#include "controller/handler_result.h"
#include "handlers/http_handler.h"

namespace cfx::handlers {

class GetHandler : public HttpHandler
{
public:
  GetHandler();
  virtual ~GetHandler();

  void handle(controller::HandlerState&) override;

protected:
  static controller::HandlerResult handleServiceTest(controller::HandlerState&) noexcept;
  static controller::HandlerResult handleReadyz(controller::HandlerState&) noexcept;
  static controller::HandlerResult handleHealthz(controller::HandlerState&) noexcept;

  controller::HandlerMap m_router;
};

} // namespace cfx::handlers

#endif // CFX_HANDLERS_GET_HANDLER_H_

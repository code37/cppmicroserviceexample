/*******************************************************************************************************************************
 * include/handlers/not_implemented_handler.h
 ******************************************************************************************************************************/

#ifndef CFX_HANDLERS_NOT_IMPLEMENTED_HANDLER_H_
#define CFX_HANDLERS_NOT_IMPLEMENTED_HANDLER_H_

#include "handlers/http_handler.h"

namespace cfx::handlers {

class NotImplementedHandler : public HttpHandler
{
public:
  NotImplementedHandler();
  virtual ~NotImplementedHandler();

  void handle(controller::HandlerState&) override;
};

} // namespace cfx::handlers

#endif // CFX_HANDLERS_NOT_IMPLEMENTED_HANDLER_H_

/*******************************************************************************************************************************
 * include/handlers/http_handler.h
 ******************************************************************************************************************************/

#ifndef CFX_HANDLERS_HTTP_HANDLER_H_
#define CFX_HANDLERS_HTTP_HANDLER_H_

#include <string>
#include <vector>

// Forward declaration
namespace web::http {
class http_request;
}

// Forward declaration
namespace cfx::controller {
class HandlerState;
}

namespace cfx::handlers {

class HttpHandler
{
public:
  HttpHandler();
  virtual ~HttpHandler();

  virtual void handle(controller::HandlerState&) = 0;

protected:
  static std::vector<std::string> requestPath(const web::http::http_request&);
};

} // namespace cfx::handlers

#endif // CFX_HANDLERS_HTTP_HANDLER_H_

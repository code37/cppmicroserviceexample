/*******************************************************************************************************************************
 * include/handlers/put_handler.h
 ******************************************************************************************************************************/

#ifndef CFX_HANDLERS_PUT_HANDLER_H_
#define CFX_HANDLERS_PUT_HANDLER_H_

#include "handlers/http_handler.h"

namespace cfx::handlers {

class PutHandler : public HttpHandler
{
public:
  PutHandler();
  virtual ~PutHandler();

  void handle(cfx::controller::HandlerState&) override;
};

} // namespace cfx::handlers

#endif // CFX_HANDLERS_PUT_HANDLER_H_

/*******************************************************************************************************************************
 * include/controller/initialize_listener_exception.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_INITIALIZE_LISTENER_EXCEPTION_H_
#define CFX_CONTROLLER_INITIALIZE_LISTENER_EXCEPTION_H_

#include <exception>

namespace cfx::controller {

class InitializeListenerException : public std::exception
{
public:
  InitializeListenerException() {}
  virtual ~InitializeListenerException() {}

  const char* what() const noexcept override
  {
    return "Failed to initialize listener";
  }
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_INITIALIZE_LISTENER_EXCEPTION_H_

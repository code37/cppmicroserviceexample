/*******************************************************************************************************************************
 * include/controller/set_endpoint_exception.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_SET_ENDPOINT_EXCEPTION_H_
#define CFX_CONTROLLER_SET_ENDPOINT_EXCEPTION_H_

#include <exception>

namespace cfx::controller {

class SetEndpointException : public std::exception
{
public:
  SetEndpointException() {}
  virtual ~SetEndpointException() {}

  const char* what() const noexcept override
  {
    return "Failed to set endpoint";
  }
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_SET_ENDPOINT_EXCEPTION_H_

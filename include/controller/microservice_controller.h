/*******************************************************************************************************************************
 * include/controller/microservice_controller.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_MICROSERVICE_CONTROLLER_H_
#define CFX_CONTROLLER_MICROSERVICE_CONTROLLER_H_

#include "controller/basic_controller.h"
#include "handlers/get_handler.h"
#include "handlers/put_handler.h"
#include "handlers/not_implemented_handler.h"
#include "utils/network_utils.h"

// Forward declaration
namespace cfx::service {
class IService;
}

namespace cfx::controller {

class MicroserviceController : public BasicController
{
public:
  MicroserviceController(service::IService&);
  virtual ~MicroserviceController();

  void setEndpoint(const utils::HostFamily, const int, const std::string&);
  void initRestOpHandlers() override;
  void handleRequest(web::http::http_request);

private:
  web::json::value responseNotImpl(const web::http::method&);

  service::IService& m_service;
  handlers::GetHandler m_getHandler;
  handlers::PutHandler m_putHandler;
  handlers::NotImplementedHandler m_notImplementedHandler;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_MICROSERVICE_CONTROLLER_H_

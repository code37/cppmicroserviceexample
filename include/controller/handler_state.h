/*******************************************************************************************************************************
 * include/controller/handler_state.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_HANDLER_STATE_H_
#define CFX_CONTROLLER_HANDLER_STATE_H_

// Forward declaration
namespace web::http {
class http_request;
}

// Forward declaration
namespace cfx::service {
class IService;
}

namespace cfx::controller {

class HandlerState
{
public:
  HandlerState(web::http::http_request& req, service::IService& svc) : m_req {req}, m_svc {svc} {}
  virtual ~HandlerState() {}

  web::http::http_request& request() noexcept { return m_req; }
  service::IService& service() noexcept { return m_svc; }

private:
  web::http::http_request& m_req;
  service::IService& m_svc;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_HANDLER_STATE_H_

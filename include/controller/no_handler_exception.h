/*******************************************************************************************************************************
 * include/controller/no_handler_exception.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_NO_HANDLER_EXCEPTION_H_
#define CFX_CONTROLLER_NO_HANDLER_EXCEPTION_H_

#include <exception>

namespace cfx::controller {

class NoHandlerException : public std::exception
{
public:
  NoHandlerException() {}
  virtual ~NoHandlerException() {}

  const char* what() const noexcept override
  {
    return "No handler";
  }
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_NO_HANDLER_EXCEPTION_H_

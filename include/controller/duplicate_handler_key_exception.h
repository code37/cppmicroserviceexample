/*******************************************************************************************************************************
 * include/controller/duplicate_handler_key_exception.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_DUPLICATE_HANDLER_KEY_EXCEPTION_H_
#define CFX_CONTROLLER_DUPLICATE_HANDLER_KEY_EXCEPTION_H_

#include <exception>
#include <string>

namespace cfx::controller {

class DuplicateHandlerKeyException : public std::exception
{
public:
  DuplicateHandlerKeyException() : m_what {"Key already exists"} {}
  DuplicateHandlerKeyException(const std::string& key) : m_what {"Key with value [" + key + "] already exists"} {}
  virtual ~DuplicateHandlerKeyException() {}

  const char* what() const noexcept override
  {
    return m_what.c_str();
  }

private:
  std::string m_what;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_DUPLICATE_HANDLER_KEY_EXCEPTION_H_

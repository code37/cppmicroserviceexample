/*******************************************************************************************************************************
 * include/controller/basic_controller.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_BASIC_CONTROLLER_H_
#define CFX_CONTROLLER_BASIC_CONTROLLER_H_

#include <string>
#include <cpprest/http_listener.h>
#include <pplx/pplxtasks.h>

namespace cfx::controller {

class BasicController
{
public:
  BasicController();
  virtual ~BasicController();

  std::string endpoint() const;
  pplx::task<void> accept();
  pplx::task<void> shutdown();

  virtual void initRestOpHandlers();

  std::vector<utility::string_t> requestPath(const web::http::http_request&);

protected:
  void initListener(const web::uri&);

  web::http::experimental::listener::http_listener m_listener;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_BASIC_CONTROLLER_H_

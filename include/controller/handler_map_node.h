/*******************************************************************************************************************************
 * include/controller/handler_map_node.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_HANDLER_MAP_NODE_H_
#define CFX_CONTROLLER_HANDLER_MAP_NODE_H_

#include <functional>
#include <set>
#include <string>
#include <vector>

#include "controller/handler_result.h"

namespace cfx::controller {

// Forward declaration
class HandlerState;

using handler_type = std::function<HandlerResult(HandlerState&)>;
using key_iter_type = std::vector<std::string>::iterator;

class HandlerMapNode
{
public:
  HandlerMapNode() : m_key {}, m_nodes {}, m_handler {} {}
  HandlerMapNode(const std::string& key) : m_key {key}, m_nodes {}, m_handler {} {}
  HandlerMapNode(const std::string& key, const handler_type& handler) : m_key {key}, m_nodes {}, m_handler {handler} {}
  HandlerMapNode(const HandlerMapNode& rhs);              // copy ctor
  HandlerMapNode(HandlerMapNode&& rhs);                   // move ctor
  HandlerMapNode& operator=(const HandlerMapNode& rhs);   // copy assignment operator
  HandlerMapNode& operator=(HandlerMapNode&& rhs);        // move assignment operator
  virtual ~HandlerMapNode() {}

  const std::string& key() const noexcept { return m_key; }
  const std::set<HandlerMapNode>& nodes() const noexcept { return m_nodes; }
  bool hasHandler() const noexcept { return bool(m_handler); }
  handler_type handler() const noexcept { return m_handler; }
  void setHandler(const handler_type& handler) noexcept { m_handler = handler; }
  HandlerMapNode& addNode(const HandlerMapNode& node);    // add node by copy
  HandlerMapNode& addNode(HandlerMapNode&& node);         // add node by move
  HandlerMapNode* findNode(const std::string& key);

  friend bool operator==(const HandlerMapNode& lhs, const HandlerMapNode& rhs) { return lhs.m_key == rhs.m_key; }
  friend bool operator==(const HandlerMapNode& lhs, const std::string& rhs) { return lhs.m_key == rhs; }
  friend bool operator==(const std::string& lhs, const HandlerMapNode& rhs) { return lhs == rhs.m_key; }
  friend bool operator!=(const HandlerMapNode& lhs, const HandlerMapNode& rhs) { return lhs.m_key != rhs.m_key; }
  friend bool operator!=(const HandlerMapNode& lhs, const std::string& rhs) { return lhs.m_key != rhs; }
  friend bool operator!=(const std::string& lhs, const HandlerMapNode& rhs) { return lhs != rhs.m_key; }
  friend bool operator<(const HandlerMapNode& lhs, const HandlerMapNode& rhs) { return lhs.m_key < rhs.m_key; }
  friend bool operator<(const HandlerMapNode& lhs, const std::string& rhs) { return lhs.m_key < rhs; }
  friend bool operator<(const std::string& lhs, const HandlerMapNode& rhs) { return lhs < rhs.m_key; }
  friend bool operator>(const HandlerMapNode& lhs, const HandlerMapNode& rhs) { return lhs.m_key > rhs.m_key; }
  friend bool operator>(const HandlerMapNode& lhs, const std::string& rhs) { return lhs.m_key > rhs; }
  friend bool operator>(const std::string& lhs, const HandlerMapNode& rhs) { return lhs > rhs.m_key; }
  friend bool operator>=(const HandlerMapNode& lhs, const HandlerMapNode& rhs) { return lhs.m_key >= rhs.m_key; }
  friend bool operator>=(const HandlerMapNode& lhs, const std::string& rhs) { return lhs.m_key >= rhs; }
  friend bool operator>=(const std::string& lhs, const HandlerMapNode& rhs) { return lhs >= rhs.m_key; }
  friend bool operator<=(const HandlerMapNode& lhs, const HandlerMapNode& rhs) { return lhs.m_key <= rhs.m_key; }
  friend bool operator<=(const HandlerMapNode& lhs, const std::string& rhs) { return lhs.m_key <= rhs; }
  friend bool operator<=(const std::string& lhs, const HandlerMapNode& rhs) { return lhs <= rhs.m_key; }

private:
  std::string m_key;
  std::set<HandlerMapNode> m_nodes;
  handler_type m_handler;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_HANDLER_MAP_NODE_H_

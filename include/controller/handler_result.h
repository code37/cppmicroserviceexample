/*******************************************************************************************************************************
 * include/controller/handler_result.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_HANDLER_RESULT_H_
#define CFX_CONTROLLER_HANDLER_RESULT_H_

#include <string>

#include "utils/usage_exception.h"

namespace cfx::controller {

class HandlerResult
{
public:
  HandlerResult() : m_handled {true}, m_reason {} {}
  HandlerResult(const std::string& reason)
    : m_handled {false}, m_reason {reason}
  {
    if (reason.empty()) {
      throw utils::UsageException("Reason cannot be an empty string");
    }
  }
  HandlerResult(const HandlerResult& rhs) : m_handled {rhs.m_handled}, m_reason {rhs.m_reason} {}  // copy ctor
  HandlerResult(HandlerResult&& rhs) : m_handled {rhs.m_handled}, m_reason {std::move(rhs.m_reason)} {}  // move ctor
  HandlerResult& operator=(const HandlerResult& rhs)  // copy assignment
  {
    m_handled = rhs.m_handled;
    m_reason = rhs.m_reason;
    return *this;
  }
  HandlerResult& operator=(HandlerResult&& rhs)       // move assignment
  {
    m_handled = rhs.m_handled;
    m_reason = std::move(rhs.m_reason);
    return *this;
  }
  virtual ~HandlerResult() {}

  bool handled() const noexcept { return m_handled; }
  const std::string& reason() const noexcept { return m_reason; }

  operator bool() const noexcept { return m_handled; }

private:
  bool m_handled;
  std::string m_reason;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_HANDLER_RESULT_H_

/*******************************************************************************************************************************
 * include/controller/duplicate_handler_path_exception.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_DUPLICATE_HANDLER_PATH_EXCEPTION_H_
#define CFX_CONTROLLER_DUPLICATE_HANDLER_PATH_EXCEPTION_H_

#include <exception>
#include <string>

namespace cfx::controller {

class DuplicateHandlerPathException : public std::exception
{
public:
  DuplicateHandlerPathException() : m_what {"Path already handled"} {}
  DuplicateHandlerPathException(const std::string& path) : m_what {"Path [" + path + "] already handled"} {}
  virtual ~DuplicateHandlerPathException() {}

  const char* what() const noexcept override
  {
    return m_what.c_str();
  }

private:
  std::string m_what;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_DUPLICATE_HANDLER_PATH_EXCEPTION_H_

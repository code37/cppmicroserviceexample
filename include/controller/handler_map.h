/*******************************************************************************************************************************
 * include/controller/handler_map.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_HANDLER_MAP_H_
#define CFX_CONTROLLER_HANDLER_MAP_H_

#include <string>
#include <vector>

#include "controller/handler_map_node.h"

namespace cfx::controller {

using path_type = std::vector<std::string>;

class HandlerMap
{
public:
  HandlerMap();
  HandlerMap(handler_type handler);
  HandlerMap(const HandlerMap& rhs);             // copy ctor
  HandlerMap(HandlerMap&& rhs);                  // move ctor
  HandlerMap& operator=(const HandlerMap& rhs);  // copy assignment operator
  HandlerMap& operator=(HandlerMap&& rhs);       // move assignment operator
  virtual ~HandlerMap() {}

  void addHandler(const path_type& path, handler_type handler);
  handler_type handler(const path_type& path);

private:
  HandlerMapNode m_root;
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_HANDLER_MAP_H_

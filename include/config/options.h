/*******************************************************************************************************************************
 * include/config/options.h
 ******************************************************************************************************************************/

#ifndef CFX_CONFIG_OPTIONS_H_
#define CFX_CONFIG_OPTIONS_H_

#include <string>

namespace cfx::config {

class Options
{
public:
  Options(int, char**);
  virtual ~Options();

  int port() const noexcept;
  std::string apiPath() const noexcept;

  static int portDefault() noexcept;
  static std::string apiPathDefault() noexcept;

private:
  int m_port;
  std::string m_api_path;

  static int s_port_default;
  static std::string s_api_path_default;
};

} // namespace cfx::config

#endif // CFX_CONFIG_OPTIONS_H_

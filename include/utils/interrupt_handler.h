/*******************************************************************************************************************************
 * include/utils/interrupt_handler.h
 ******************************************************************************************************************************/

#ifndef CFX_UTILS_INTERRUPT_HANDLER_H_
#define CFX_UTILS_INTERRUPT_HANDLER_H_

namespace cfx::utils {

class InterruptHandler
{
public:
  static void hookSignals();
  static void handleSignalInterrupt(const int);
  static void waitForSignalInterrupt();

private:
  InterruptHandler() {}
};

} // namespace cfx::utils

#endif // CFX_UTILS_INTERRUPT_HANDLER_H_

/*******************************************************************************************************************************
 * include/utils/usage_exception.h
 ******************************************************************************************************************************/

#ifndef CFX_UTILS_USAGE_EXCEPTION_H_
#define CFX_UTILS_USAGE_EXCEPTION_H_

#include <exception>
#include <string>

namespace cfx::utils {

class UsageException : public std::exception
{
public:
  UsageException() : m_what {"Incorrect usage"} {}
  UsageException(const std::string& reason) : m_what {"Incorrect usage: " + reason} {}
  virtual ~UsageException() {}

  const char* what() const noexcept override
  {
    return m_what.c_str();
  }

private:
  std::string m_what;
};

} // namespace cfx::utils

#endif // CFX_UTILS_USAGE_EXCEPTION_H_

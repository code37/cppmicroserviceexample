/*******************************************************************************************************************************
 * include/utils/network_utils.h
 ******************************************************************************************************************************/

#ifndef CFX_UTILS_NETWORK_UTILS_H_
#define CFX_UTILS_NETWORK_UTILS_H_

#include <string>
#include <boost/asio.hpp>

namespace cfx::utils {

enum class HostFamily { Ip4, Ip6, };

class NetworkUtils
{
public:
  static std::string hostIp4();
  static std::string hostIp6();
  static std::string hostName();
  static std::string host(const HostFamily);

private:
  NetworkUtils() {}

  static boost::asio::ip::tcp::resolver::iterator queryHostInetInfo();
  static std::string hostIp(unsigned short);
};

} // namespace cfx::utils

#endif // CFX_UTILS_NETWORK_UTILS_H_

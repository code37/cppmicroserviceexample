/*******************************************************************************************************************************
 * include/utils/logging.h
 ******************************************************************************************************************************/

#ifndef CFX_UTILS_LOGGING_H_
#define CFX_UTILS_LOGGING_H_

#include <string>
#include <spdlog/logger.h>

namespace cfx::utils {

enum class LogFormat { Default, Debug, Json, };

enum class LogLevel { Default, Off, Info, Debug, Trace, };

class Logging
{
public:
  static void init(const LogFormat = LogFormat::Default, const LogLevel = LogLevel::Default);
  static void setFormat(const LogFormat);
  static void setLevel(const LogLevel);

  static void trace(const std::string&);
  static void debug(const std::string&);
  static void info(const std::string&);
  static void warn(const std::string&);
  static void error(const std::string&);

private:
  static bool s_initialized;
};

} // namespace cfx::utils

typedef cfx::utils::Logging Logger;

#endif // CFX_UTILS_LOGGING_H_

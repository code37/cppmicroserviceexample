/*******************************************************************************************************************************
 * include/service/iservice.h
 ******************************************************************************************************************************/

#ifndef CFX_SERVICE_ISERVICE_H_
#define CFX_SERVICE_ISERVICE_H_

namespace cfx::service {

enum class ReadyStatus { Shutdown, Starting, Ready, Error };

class IService
{
public:
  virtual ~IService() {}

  virtual void startup() = 0;
  virtual void shutdown() = 0;
  virtual ReadyStatus ready() = 0;
};

} // namespace cfx::service

#endif // CFX_SERVICE_ISERVICE_H_

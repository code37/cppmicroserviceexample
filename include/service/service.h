/*******************************************************************************************************************************
 * include/service/service.h
 ******************************************************************************************************************************/

#ifndef CFX_SERVICE_SERVICE_H_
#define CFX_SERVICE_SERVICE_H_

#include "service/iservice.h"

namespace cfx::service {

class Service : public IService
{
public:
  Service();
  virtual ~Service();

  void startup() override;
  void shutdown() override;
  ReadyStatus ready() override { return m_ready_status; }

private:
  ReadyStatus m_ready_status;
};

} // namespace cfx::service

#endif // CFX_SERVICE_SERVICE_H_

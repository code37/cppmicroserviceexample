/*******************************************************************************************************************************
 * include/service/service_mock.h
 ******************************************************************************************************************************/

#ifndef CFX_SERVICE_SERVICE_MOCK_H_
#define CFX_SERVICE_SERVICE_MOCK_H_

#include "service/iservice.h"

namespace cfx::service {

class ServiceMock : public IService
{
public:
  ServiceMock();
  virtual ~ServiceMock();

  void startup() override;
  void shutdown() override;
  ReadyStatus ready() override { return m_ready_status; }

  void set_ready(ReadyStatus rs) { m_ready_status = rs; }

private:
  ReadyStatus m_ready_status;
};

} // namespace cfx::service

#endif // CFX_SERVICE_SERVICE_MOCK_H_

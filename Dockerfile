FROM ubuntu:20.04

RUN apt-get update -y && \
    apt-get install -y libcpprest2.10 libboost-program-options1.71.0 libspdlog1 libssl1.1

ADD ./bin/Release/run_service /usr/local/bin/

ENTRYPOINT ["run_service"]

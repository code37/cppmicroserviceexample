/*******************************************************************************************************************************
 * src/service/service.cpp
 ******************************************************************************************************************************/

#include <chrono>
#include <thread>

#include "service/service.h"
#include "utils/logging.h"

namespace cfx::service {

Service::Service()
  : m_ready_status { ReadyStatus::Shutdown }
{
}

Service::~Service()
{
}

void Service::startup()
{
  try {
    m_ready_status = ReadyStatus::Starting;
    Logger::info("Ready status changed to Starting");
    std::this_thread::sleep_for(std::chrono::seconds(10));
    m_ready_status = ReadyStatus::Ready;
    Logger::info("Ready status changed to Ready");
  }
  catch (std::exception& ex) {
    m_ready_status = ReadyStatus::Error;
    Logger::error("Exception while starting service");
  }
}

void Service::shutdown()
{
}

} // namespace cfx::service

/*******************************************************************************************************************************
 * src/main.cpp
 ******************************************************************************************************************************/

#include <iostream>
#include <thread>

#include "config/options.h"
#include "controller/microservice_controller.h"
#include "service/service.h"
#include "utils/interrupt_handler.h"
#include "utils/logging.h"

int main(int argc, char** argv)
{
  cfx::config::Options opts(argc, argv);

  Logger::init();

#ifndef NO_SIGNAL_TRAP
  cfx::utils::InterruptHandler::hookSignals();
#endif // NO_SIGNAL_TRAP

  cfx::service::Service service;
  std::thread service_startup_t(&cfx::service::Service::startup, &service);

  cfx::controller::MicroserviceController http_server(service);
  http_server.setEndpoint(cfx::utils::HostFamily::Ip4, opts.port(), opts.apiPath());

  try {
    // wait for server initialization
    http_server.accept().wait();

    Logger::info("C++ microservice now listening for requests at: " + http_server.endpoint());

#ifdef NO_SIGNAL_TRAP
    std::cout << "Enter \"q\" to stop the microservice ";
    std::string nst_input {};
    std::cin >> nst_input;
#else
    cfx::utils::InterruptHandler::waitForSignalInterrupt();
#endif // NO_SIGNAL_TRAP

    http_server.shutdown().wait();
  }
  catch (std::exception& ex) {
    std::cerr << "EXCEPTION: type std::exception in main" << std::endl;
  }
  catch (...) {
    std::cerr << "EXCEPTION: type ... in main " << std::endl;
  }

  service_startup_t.join();

  service.shutdown();

  return 0;
}

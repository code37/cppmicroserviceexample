/*******************************************************************************************************************************
 * src/config/options.cpp
 ******************************************************************************************************************************/

#include <iostream>
#include <boost/program_options.hpp>

#include "config/options.h"

namespace bpo = boost::program_options;

namespace cfx::config {

int Options::s_port_default {8000};
std::string Options::s_api_path_default {"/api"};

Options::Options(int argc, char** argv)
  : m_port {-1}, m_api_path {}
{
  bpo::options_description desc("Allowed options");
  desc.add_options()
    ("help", "show help message")
    ("api-path,P", bpo::value<std::string>(), "endpoint api path")
    ("port,p", bpo::value<int>(), "endpoint port")
  ;

  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(argc, argv, desc), vm);
  bpo::notify(vm);

  m_port = (vm.count("port")) ? vm["port"].as<int>() : Options::portDefault();
  m_api_path = (vm.count("api-path")) ? vm["api-path"].as<std::string>() : Options::apiPathDefault();
}

Options::~Options()
{
}

int Options::port() const noexcept
{
  return m_port;
}

int Options::portDefault() noexcept
{
  return s_port_default;
}

std::string Options::apiPath() const noexcept
{
  return m_api_path;
}

std::string Options::apiPathDefault() noexcept
{
  return s_api_path_default;
}

} // namespace cfx::config

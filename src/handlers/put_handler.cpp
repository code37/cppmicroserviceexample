/*******************************************************************************************************************************
 * src/handlers/put_handler.cpp
 ******************************************************************************************************************************/

#include "controller/handler_state.h"
#include "handlers/put_handler.h"

namespace cfx::handlers {

PutHandler::PutHandler()
{
}

PutHandler::~PutHandler()
{
}

void PutHandler::handle(controller::HandlerState& state)
{
}

} // namespace cfx::handlers

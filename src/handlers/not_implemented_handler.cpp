/*******************************************************************************************************************************
 * src/handlers/not_implemented_handler.cpp
 ******************************************************************************************************************************/

#include <cpprest/http_msg.h>

#include "constants/constants.h"
#include "controller/handler_state.h"
#include "handlers/not_implemented_handler.h"

namespace cfx::handlers {

NotImplementedHandler::NotImplementedHandler()
{
}

NotImplementedHandler::~NotImplementedHandler()
{
}

void NotImplementedHandler::handle(controller::HandlerState& state)
{
  auto request = state.request();
  auto response = web::json::value::object();
  response["service_name"] = web::json::value::string(constants::ProjectName);
  response["http_method"] = web::json::value::string(request.method());
  request.reply(web::http::status_codes::NotImplemented, response);
}

} // namespace cfx::handlers

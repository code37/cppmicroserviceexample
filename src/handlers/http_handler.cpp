/*******************************************************************************************************************************
 * src/handlers/http_handler.cpp
 ******************************************************************************************************************************/

#include <cpprest/http_msg.h>

#include "handlers/http_handler.h"

namespace cfx::handlers {

HttpHandler::HttpHandler()
{
}

HttpHandler::~HttpHandler()
{
}

std::vector<std::string> HttpHandler::requestPath(const web::http::http_request& message)
{
  auto relativePath = web::uri::decode(message.relative_uri().path());

  return web::uri::split_path(relativePath);
}

} // namespace cfx::handlers

/*******************************************************************************************************************************
 * src/handlers/get_handler.cpp
 ******************************************************************************************************************************/

#include <cpprest/http_msg.h>

#include "constants/constants.h"
#include "constants/version.h"
#include "controller/handler_map.h"
#include "controller/handler_state.h"
#include "handlers/get_handler.h"
#include "service/iservice.h"
#include "utils/logging.h"

namespace cfx::handlers {

GetHandler::GetHandler()
  : m_router {}
{
  m_router.addHandler({"service", "test"}, handleServiceTest);
  m_router.addHandler({"readyz"}, handleReadyz);
  m_router.addHandler({"healthz"}, handleHealthz);
}

GetHandler::~GetHandler()
{
}

void GetHandler::handle(controller::HandlerState& state)
{
  auto req = state.request();
  std::vector<std::string> path = requestPath(req);
  auto handler = m_router.handler(path);
  controller::HandlerResult hresult {"No handler"};
  if (handler) {
    hresult = handler(state);
  } else {
    auto response = web::json::value::object();
    response["service_name"] = web::json::value::string(constants::ProjectName);
    response["http_method"] = web::json::value::string(req.method());
    response["path"] = web::json::value::string(web::uri::decode(req.relative_uri().path()));
    req.reply(web::http::status_codes::NotFound, response);
  }
}

controller::HandlerResult GetHandler::handleServiceTest(controller::HandlerState& state) noexcept
{
  auto req = state.request();
  auto response = web::json::value::object();
  response["service_name"] = web::json::value::string(constants::ProjectName);
  response["timestamp"] = web::json::value::string(constants::ProjectBuildTimestamp);
  response["commit"] = web::json::value::string(constants::ProjectCommitHash);
  response["version"] = web::json::value::string(constants::ProjectSemanticVersion);
  response["status"] = web::json::value::string("ready!");
  req.reply(web::http::status_codes::OK, response);

  return {};
}

controller::HandlerResult GetHandler::handleReadyz(controller::HandlerState& state) noexcept
{
  if (state.service().ready() == service::ReadyStatus::Ready) {
    state.request().reply(web::http::status_codes::OK);
  } else {
    state.request().reply(web::http::status_codes::ServiceUnavailable);
  }

  return {};
}

controller::HandlerResult GetHandler::handleHealthz(controller::HandlerState& state) noexcept
{
  state.request().reply(web::http::status_codes::OK);

  return {};
}

} // namespace cfx::handlers

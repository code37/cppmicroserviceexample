/*******************************************************************************************************************************
 * utils/network_utils.cpp
 ******************************************************************************************************************************/

#include "utils/network_utils.h"

namespace cfx::utils {

std::string NetworkUtils::hostIp4()
{
  return hostIp(AF_INET);
}

std::string NetworkUtils::hostIp6()
{
  return hostIp(AF_INET6);
}

std::string NetworkUtils::hostName()
{
  return boost::asio::ip::host_name();
}

std::string NetworkUtils::host(const HostFamily hfam)
{
  switch (hfam) {
  case HostFamily::Ip4:
    return NetworkUtils::hostIp4();
  case HostFamily::Ip6:
    return NetworkUtils::hostIp6();
  default:
    throw "HostFamily option not implemented";
  }
}

boost::asio::ip::tcp::resolver::iterator NetworkUtils::queryHostInetInfo()
{
  boost::asio::io_context iox;
  boost::asio::ip::tcp::resolver resolver(iox);
  boost::asio::ip::tcp::resolver::query qry(boost::asio::ip::host_name(), "");

  return resolver.resolve(qry);
}

std::string NetworkUtils::hostIp(unsigned short family)
{
  auto hostInetInfo = queryHostInetInfo();
  boost::asio::ip::tcp::resolver::iterator end;
  while (hostInetInfo != end) {
    boost::asio::ip::tcp::endpoint ep = *hostInetInfo++;
    sockaddr sa = *ep.data();
    if (sa.sa_family == family) {

      return ep.address().to_string();
    }
  }

  return "";
}

} // namespace cfx::utils

/*******************************************************************************************************************************
 * utils/logging.cpp
 ******************************************************************************************************************************/

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_sinks.h>

#include "utils/logging.h"

namespace cfx::utils {

const char stdout_logger[] {"stdout_logger"};
const char stderr_logger[] {"stderr_logger"};

bool Logging::s_initialized {false};

void Logging::init(const LogFormat format, const LogLevel level)
{
  Logging::s_initialized = true;
  spdlog::stdout_logger_mt(stdout_logger);
  spdlog::stderr_logger_mt(stderr_logger);
  setFormat(format);
  setLevel(level);
}

void Logging::setFormat(const LogFormat format)
{
  switch (format) {
  case LogFormat::Default:
    spdlog::set_pattern("[%Y-%m-%d %H:%M:%S.%f] [%n] [%l] %v");
    break;
  case LogFormat::Debug:
    spdlog::set_pattern("[%n] [%l] %v");
    break;
  case LogFormat::Json:
    spdlog::set_pattern(
      "{\"source\":\"%n\","
      "\"details\":{"
        "\"level\":\"%l\","
        "\"name\":\"%n\","
        "\"timestamp\":\"%Y-%m-%d %H:%M:%S.%f\","
        "\"message\":\"%v\"}}");
    break;
  default:
    throw "LogFormat option not implemented";
  }
}

void Logging::setLevel(const LogLevel level)
{
  switch (level) {
  case LogLevel::Off:
    spdlog::get(stdout_logger)->set_level(spdlog::level::off);
    spdlog::get(stderr_logger)->set_level(spdlog::level::off);
    break;
  case LogLevel::Default:
  case LogLevel::Info:
    spdlog::get(stdout_logger)->set_level(spdlog::level::info);
    spdlog::get(stderr_logger)->set_level(spdlog::level::info);
    break;
  case LogLevel::Debug:
    spdlog::get(stdout_logger)->set_level(spdlog::level::debug);
    spdlog::get(stderr_logger)->set_level(spdlog::level::debug);
    break;
  case LogLevel::Trace:
    spdlog::get(stdout_logger)->set_level(spdlog::level::trace);
    spdlog::get(stderr_logger)->set_level(spdlog::level::trace);
    break;
  default:
    throw "LogLevel option not implemented";
  }
}

void Logging::trace(const std::string& msg)
{
  spdlog::get(stdout_logger)->trace(msg);
}

void Logging::debug(const std::string& msg)
{
  spdlog::get(stdout_logger)->debug(msg);
}

void Logging::info(const std::string& msg)
{
  spdlog::get(stdout_logger)->info(msg);
}

void Logging::warn(const std::string& msg)
{
  spdlog::get(stdout_logger)->warn(msg);
}

void Logging::error(const std::string& msg)
{
  spdlog::get(stderr_logger)->error(msg);
}

} // namespace cfx::utils

/*******************************************************************************************************************************
 * utils/interrupt_handler.cpp
 ******************************************************************************************************************************/

#include <condition_variable>
#include <iostream>
#include <mutex>
#include <signal.h>

#include "utils/interrupt_handler.h"
#include "utils/logging.h"

static std::condition_variable s_condition;
static std::mutex s_mutex;

namespace cfx::utils {

void InterruptHandler::hookSignals()
{
  signal(SIGHUP, handleSignalInterrupt);
  signal(SIGINT, handleSignalInterrupt);
  signal(SIGTERM, handleSignalInterrupt);
}

void InterruptHandler::handleSignalInterrupt(const int signal)
{
  switch (signal) {
  case SIGHUP:
    Logger::info("SIGHUP trapped ...");
    break;
  case SIGINT:
    Logger::info("SIGINT trapped ...");
    s_condition.notify_one();
    break;
  case SIGTERM:
    Logger::info("SIGTERM trapped ...");
    s_condition.notify_one();
    break;
  }
}

void InterruptHandler::waitForSignalInterrupt()
{
  std::unique_lock<std::mutex> lock {s_mutex};
  s_condition.wait(lock);
  Logger::info("Received signal to interrupt program ...");
  lock.unlock();
}

} // namespace cfx::utils

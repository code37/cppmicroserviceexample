/*******************************************************************************************************************************
 * src/controller/handler_map_node.cpp
 ******************************************************************************************************************************/

#include "controller/duplicate_handler_key_exception.h"
#include "controller/duplicate_handler_path_exception.h"
#include "controller/handler_map_node.h"
#include "controller/no_handler_exception.h"

namespace cfx::controller {

HandlerMapNode::HandlerMapNode(const HandlerMapNode& rhs)
  : m_key {rhs.m_key}, m_nodes {rhs.m_nodes}, m_handler {rhs.m_handler}
{
}

HandlerMapNode::HandlerMapNode(HandlerMapNode&& rhs)
  : m_key {std::move(rhs.m_key)}, m_nodes {std::move(rhs.m_nodes)}, m_handler {std::move(rhs.m_handler)}
{
}

HandlerMapNode& HandlerMapNode::operator=(const HandlerMapNode& rhs)
{
  m_key = rhs.m_key;
  m_nodes = rhs.m_nodes;
  m_handler = rhs.m_handler;

  return *this;
}

HandlerMapNode& HandlerMapNode::operator=(HandlerMapNode&& rhs)
{
  m_key = std::move(rhs.m_key);
  m_nodes = std::move(rhs.m_nodes);
  m_handler = std::move(rhs.m_handler);

  return *this;
}

HandlerMapNode& HandlerMapNode::addNode(const HandlerMapNode& node)
{
  std::pair<std::set<HandlerMapNode>::iterator, bool> result = m_nodes.insert(node);
  if (result.second == false) {
    throw DuplicateHandlerKeyException(node.m_key);
  }

  return const_cast<HandlerMapNode&>(*result.first);
}

HandlerMapNode& HandlerMapNode::addNode(HandlerMapNode&& node)
{
  std::pair<std::set<HandlerMapNode>::iterator, bool> result = m_nodes.insert(std::move(node));
  if (result.second == false) {
    throw DuplicateHandlerKeyException(node.m_key);
  }

  return const_cast<HandlerMapNode&>(*result.first);
}

HandlerMapNode* HandlerMapNode::findNode(const std::string& key)
{
  std::set<HandlerMapNode>::iterator inode = m_nodes.find(key);
  if (inode != m_nodes.end()) {

    return const_cast<HandlerMapNode*>(&(*inode));
  }

  return nullptr;
}

} // namespace cfx::controller

/*******************************************************************************************************************************
 * src/controller/handler_map.cpp
 ******************************************************************************************************************************/

#include "controller/handler_map.h"

namespace cfx::controller {

HandlerMap::HandlerMap()
  : m_root {"/"}
{
}

HandlerMap::HandlerMap(handler_type handler)
  : m_root {"/", handler}
{
}

HandlerMap::HandlerMap(const HandlerMap& rhs)
  : m_root {rhs.m_root}
{
}

HandlerMap::HandlerMap(HandlerMap&& rhs)
  : m_root {std::move(rhs.m_root)}
{
}

HandlerMap& HandlerMap::operator=(const HandlerMap& rhs)
{
  m_root = rhs.m_root;

  return *this;
}

HandlerMap& HandlerMap::operator=(HandlerMap&& rhs)
{
  m_root = std::move(rhs.m_root);

  return *this;
}

void HandlerMap::addHandler(const path_type& path, handler_type handler)
{
  HandlerMapNode* pnode = &m_root;
  for (auto key : path) {
    HandlerMapNode* pnext = pnode->findNode(key);
    if (pnext) {
      pnode = pnext;
    } else {
      pnode = &pnode->addNode(key);
    }
  }
  pnode->setHandler(handler);
}

handler_type HandlerMap::handler(const path_type& path)
{
  if (path.empty() || (path.size() == 1 && path[0] == "/")) {
    return m_root.handler();
  }

  HandlerMapNode* pnode = &m_root;
  for (auto key : path) {
    pnode = pnode->findNode(key);
    if (!pnode) {
      break;
    }
  }
  if (pnode) {

    return pnode->handler();
  }

  return {};
}

} // namespace cfx::controller

/*******************************************************************************************************************************
 * src/controller/microservice_controller.cpp
 ******************************************************************************************************************************/

#include <sstream>

#include "controller/handler_state.h"
#include "controller/microservice_controller.h"
#include "controller/set_endpoint_exception.h"
#include "utils/logging.h"
#include "utils/network_utils.h"

namespace cfx::controller {

MicroserviceController::MicroserviceController(service::IService& svc)
  : m_service(svc), m_getHandler(), m_putHandler(), m_notImplementedHandler()
{
}

MicroserviceController::~MicroserviceController()
{
}

void MicroserviceController::setEndpoint(const utils::HostFamily hfam, const int port, const std::string& path)
{
  std::string host = utils::NetworkUtils::host(hfam);
  web::uri_builder endpointBuilder;
  web::uri uri;
  try {
    endpointBuilder.set_scheme("http").set_host(host).set_port(port).set_path(path);
    uri = endpointBuilder.to_uri();
  }
  catch (const std::exception& ex) {
    std::stringstream ss1;
    ss1 << "Error setting endpoint URI: " << ex.what();
    Logger::error(ss1.str());
    throw SetEndpointException();
  }
  std::stringstream ss2;
  ss2 << "Endpoint set to: " << uri.to_string();
  Logger::info(ss2.str());
  BasicController::initListener(uri);
}

void MicroserviceController::handleRequest(web::http::http_request req)
{
  controller::HandlerState state {req, m_service};
  if (req.method() == web::http::methods::GET) {
    m_getHandler.handle(state);
  } else {
    m_notImplementedHandler.handle(state);
  }
}

void MicroserviceController::initRestOpHandlers()
{
  m_listener.support(std::bind(&MicroserviceController::handleRequest, this, std::placeholders::_1));
}

} // namespace cfx::controller

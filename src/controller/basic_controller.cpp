/*******************************************************************************************************************************
 * src/controller/basic_controller.cpp
 ******************************************************************************************************************************/

#include <sstream>

#include "controller/basic_controller.h"
#include "controller/initialize_listener_exception.h"
#include "utils/logging.h"

namespace cfx::controller {

BasicController::BasicController()
  : m_listener()
{
}

BasicController::~BasicController()
{
}

std::string BasicController::endpoint() const
{
  return m_listener.uri().to_string();
}

pplx::task<void> BasicController::accept()
{
  initRestOpHandlers();

  return m_listener.open();
}

pplx::task<void> BasicController::shutdown()
{
  return m_listener.close();
}

void BasicController::initRestOpHandlers()
{
}

std::vector<utility::string_t> BasicController::requestPath(const web::http::http_request& message)
{
  auto relativePath = web::uri::decode(message.relative_uri().path());

  return web::uri::split_path(relativePath);
}

void BasicController::initListener(const web::uri& uri)
{
  try {
    m_listener = web::http::experimental::listener::http_listener(uri);
  }
  catch (const std::exception& ex) {
    std::stringstream ss;
    ss << "Failed to initialize listener: " << ex.what();
    Logger::error(ss.str());
    throw InitializeListenerException();
  }
}

} // namespace cfx::controller

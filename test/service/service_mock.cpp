/*******************************************************************************************************************************
 * test/service/service_mock.cpp
 ******************************************************************************************************************************/

#include "service/service_mock.h"

namespace cfx::service {

ServiceMock::ServiceMock()
  : m_ready_status { ReadyStatus::Shutdown }
{
}

ServiceMock::~ServiceMock()
{
}

void ServiceMock::startup()
{
}

void ServiceMock::shutdown()
{
}

} // namespace cfx::service

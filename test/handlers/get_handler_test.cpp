/*******************************************************************************************************************************
 * test/handlers/get_handler_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <cpprest/http_msg.h>
#include <pplx/pplxtasks.h>

#include "constants/constants.h"
#include "controller/handler_state.h"
#include "handlers/get_handler.h"
#include "service/service_mock.h"

namespace cfx::handlers {

static const std::string contentTypeKey {"Content-Type"};
static const std::string appJsonValue {"application/json"};
static const std::string serviceNameKey {"service_name"};
static const std::string httpMethodKey {"http_method"};
static const std::string timestampKey {"timestamp"};
static const std::string commitKey {"commit"};
static const std::string versionKey {"version"};
static const std::string statusKey {"status"};
static const std::string pathKey {"path"};
static const std::string expectedServiceName {cfx::constants::ProjectName};

class GetHandlerTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}

  static bool validateField(const web::json::value&, const std::string&, const std::string&);
  static bool validateStringFieldExists(const web::json::value&, const std::string&);
  static bool validateHeader(web::http::http_headers, const std::string&, const std::string&);
};

TEST_F(GetHandlerTest, valid_service_test_response)
{
  GetHandler tgt;
  web::http::http_request req(web::http::methods::GET);
  req.set_request_uri("/service/test");
  service::ServiceMock svc;
  controller::HandlerState state(req, svc);
  tgt.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::OK, status);

  // Get and test the header
  web::http::http_headers headers = resp.headers();
  EXPECT_PRED3(validateHeader, headers, contentTypeKey, appJsonValue);

  // Get the JSON body and test the fields
  pplx::task<web::json::value> body_task = resp.extract_json();
  web::json::value body = body_task.get();
  EXPECT_PRED3(validateField, body, serviceNameKey, expectedServiceName);
  EXPECT_PRED2(validateStringFieldExists, body, timestampKey);
  EXPECT_PRED2(validateStringFieldExists, body, commitKey);
  EXPECT_PRED2(validateStringFieldExists, body, versionKey);
  EXPECT_PRED2(validateStringFieldExists, body, statusKey);
}

TEST_F(GetHandlerTest, readyz_handler_responds_unavailable_when_service_shutdown)
{
  GetHandler tgt;
  web::http::http_request req(web::http::methods::GET);
  req.set_request_uri("/readyz");
  service::ServiceMock svc;
  svc.set_ready(service::ReadyStatus::Shutdown);
  controller::HandlerState state(req, svc);
  tgt.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::ServiceUnavailable, status);
}

TEST_F(GetHandlerTest, readyz_handler_responds_unavailable_when_service_starting)
{
  GetHandler tgt;
  web::http::http_request req(web::http::methods::GET);
  req.set_request_uri("/readyz");
  service::ServiceMock svc;
  svc.set_ready(service::ReadyStatus::Starting);
  controller::HandlerState state(req, svc);
  tgt.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::ServiceUnavailable, status);
}

TEST_F(GetHandlerTest, readyz_handler_responds_unavailable_when_service_error)
{
  GetHandler tgt;
  web::http::http_request req(web::http::methods::GET);
  req.set_request_uri("/readyz");
  service::ServiceMock svc;
  svc.set_ready(service::ReadyStatus::Error);
  controller::HandlerState state(req, svc);
  tgt.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::ServiceUnavailable, status);
}

TEST_F(GetHandlerTest, readyz_handler_responds_ok_when_service_ready)
{
  GetHandler tgt;
  web::http::http_request req(web::http::methods::GET);
  req.set_request_uri("/readyz");
  service::ServiceMock svc;
  svc.set_ready(service::ReadyStatus::Ready);
  controller::HandlerState state(req, svc);
  tgt.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::OK, status);
}

TEST_F(GetHandlerTest, nonexistant_handler_response)
{
  GetHandler tgt;
  web::http::http_request req(web::http::methods::GET);
  req.set_request_uri("/some/unhandled/uri/like/this/one");
  service::ServiceMock svc;
  controller::HandlerState state(req, svc);
  tgt.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::NotFound, status);

  // Get and test the header
  web::http::http_headers headers = resp.headers();
  EXPECT_PRED3(validateHeader, headers, contentTypeKey, appJsonValue);

  // Get the JSON body and test the fields
  pplx::task<web::json::value> body_task = resp.extract_json();
  web::json::value body = body_task.get();
  EXPECT_PRED3(validateField, body, serviceNameKey, expectedServiceName);
  EXPECT_PRED3(validateField, body, httpMethodKey, "GET");
  EXPECT_PRED3(validateField, body, pathKey, "/some/unhandled/uri/like/this/one");
}

bool GetHandlerTest::validateField(const web::json::value& body, const std::string& key, const std::string& value)
{
  bool valid {false};
  if (body.has_field(key)) {
    web::json::value jval = body.at(key);
    if (jval.is_string()) {
      std::string sval = jval.as_string();
      valid = (sval.compare(value) == 0);
    }
  }

  return valid;
}

bool GetHandlerTest::validateStringFieldExists(const web::json::value& body, const std::string& key)
{
  bool valid {false};
  if (body.has_field(key)) {
    web::json::value jval = body.at(key);
    if (jval.is_string()) {
      valid = true;
    }
  }

  return valid;
}

bool GetHandlerTest::validateHeader(web::http::http_headers headers, const std::string& key, const std::string& value)
{
  bool valid {false};
  if (headers.has(key)) {
    valid = (value.compare(headers[key]) == 0);
  }

  return valid;
}

} // namespace cfx::handlers

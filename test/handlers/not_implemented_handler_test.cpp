/*******************************************************************************************************************************
 * test/handlers/not_implemented_handler_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <cpprest/http_msg.h>
#include <pplx/pplxtasks.h>

#include "constants/constants.h"
#include "controller/handler_state.h"
#include "handlers/not_implemented_handler.h"
#include "service/service_mock.h"

namespace cfx::handlers {

static const std::string contentTypeKey {"Content-Type"};
static const std::string appJsonValue {"application/json"};
static const std::string serviceNameKey {"service_name"};
static const std::string httpMethodKey {"http_method"};
static const std::string expectedServiceName {cfx::constants::ProjectName};

class NotImplementedHandlerTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}

  static bool validateField(const web::json::value&, const std::string&, const std::string&);
  static bool validateHeader(web::http::http_headers, const std::string&, const std::string&);
};

TEST_F(NotImplementedHandlerTest, check_response_put)
{
  NotImplementedHandler target;
  web::http::http_request req(web::http::methods::PUT);
  service::ServiceMock svc;
  controller::HandlerState state(req, svc);
  target.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::NotImplemented, status);

  // Get and test the header
  web::http::http_headers headers = resp.headers();
  EXPECT_PRED3(validateHeader, headers, contentTypeKey, appJsonValue);

  // Get the JSON body and test the fields
  pplx::task<web::json::value> body_task = resp.extract_json();
  web::json::value body = body_task.get();
  EXPECT_PRED3(validateField, body, serviceNameKey, expectedServiceName);
  EXPECT_PRED3(validateField, body, httpMethodKey, "PUT");
}

TEST_F(NotImplementedHandlerTest, check_response_delete)
{
  NotImplementedHandler target;
  web::http::http_request req(web::http::methods::DEL);
  service::ServiceMock svc;
  controller::HandlerState state(req, svc);
  target.handle(state);
  pplx::task<web::http::http_response> resp_task = req.get_response();
  // Get the response
  web::http::http_response resp = resp_task.get();

  // Test the response status code
  web::http::status_code status = resp.status_code();
  EXPECT_EQ(web::http::status_codes::NotImplemented, status);

  // Get and test the header
  web::http::http_headers headers = resp.headers();
  EXPECT_PRED3(validateHeader, headers, contentTypeKey, appJsonValue);

  // Get the JSON body and test the fields
  pplx::task<web::json::value> body_task = resp.extract_json();
  web::json::value body = body_task.get();
  EXPECT_PRED3(validateField, body, serviceNameKey, expectedServiceName);
  EXPECT_PRED3(validateField, body, httpMethodKey, "DELETE");
}

bool NotImplementedHandlerTest::validateField(const web::json::value& body, const std::string& key, const std::string& value)
{
  bool valid {false};
  if (body.has_field(key)) {
    web::json::value jval = body.at(key);
    if (jval.is_string()) {
      std::string sval = jval.as_string();
      valid = (sval.compare(value) == 0);
    }
  }

  return valid;
}

bool NotImplementedHandlerTest::validateHeader(web::http::http_headers headers, const std::string& key,
                                               const std::string& value)
{
  bool valid {false};
  if (headers.has(key)) {
    valid = (value.compare(headers[key]) == 0);
  }

  return valid;
}

} // namespace cfx::handlers

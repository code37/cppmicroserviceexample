/*******************************************************************************************************************************
 * utils/network_utils_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "utils/network_utils.h"

namespace cfx::utils {

class NetworkUtilsTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(NetworkUtilsTest, hostIp4_no_throw)
{
  // Depending on the machine, the function may or may not return a testable value
  EXPECT_NO_THROW(NetworkUtils::hostIp4());
}

TEST_F(NetworkUtilsTest, hostIp6_no_throw)
{
  // Depending on the machine, the function may or may not return a testable value
  EXPECT_NO_THROW(NetworkUtils::hostIp6());
}

TEST_F(NetworkUtilsTest, host_no_throw)
{
  // Depending on the machine, the function may or may not return a testable value
  EXPECT_NO_THROW(NetworkUtils::host(HostFamily::Ip4));
}

TEST_F(NetworkUtilsTest, hostName_not_empty)
{
  std::string retval {NetworkUtils::hostName()};
  EXPECT_LT(0UL, retval.size());
}

} // namespace cfx::utils

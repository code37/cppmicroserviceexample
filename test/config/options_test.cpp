/*******************************************************************************************************************************
 * test/config/options_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "config/options.h"

namespace cfx::config {

class OptionsTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(OptionsTest, port_option_long)
{
  int argc {2};
  const char* argv[] {"progname", "--port=8091"};
  Options target(argc, const_cast<char**>(argv));
  EXPECT_EQ(8091, target.port());
}

TEST_F(OptionsTest, port_option_short)
{
  int argc {2};
  const char* argv[] {"progname", "-p8900"};
  Options target(argc, const_cast<char**>(argv));
  EXPECT_EQ(8900, target.port());
}

TEST_F(OptionsTest, port_default)
{
  int argc {1};
  const char* argv[] {"progname"};
  Options target(argc, const_cast<char**>(argv));
  EXPECT_EQ(Options::portDefault(), target.port());
}

TEST_F(OptionsTest, apiPath_option_long)
{
  int argc {2};
  const char* argv[] {"progname", "--api-path=/path/to/api"};
  Options target(argc, const_cast<char**>(argv));
  EXPECT_EQ("/path/to/api", target.apiPath());
}

TEST_F(OptionsTest, apiPath_option_short)
{
  int argc {2};
  const char* argv[] {"progname", "-P/your/api/path/here"};
  Options target(argc, const_cast<char**>(argv));
  EXPECT_EQ("/your/api/path/here", target.apiPath());
}

TEST_F(OptionsTest, apiPath_default)
{
  int argc {1};
  const char* argv[] {"progname"};
  Options target(argc, const_cast<char**>(argv));
  EXPECT_EQ(Options::apiPathDefault(), target.apiPath());
}

} // namespace cfx::config


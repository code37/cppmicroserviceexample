/*******************************************************************************************************************************
 * test/controller/handler_map_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <cpprest/http_msg.h>

#include "controller/handler_map.h"
#include "controller/handler_state.h"
#include "service/service_mock.h"

namespace cfx::controller {

class HandlerMapTest : public ::testing::Test
{
protected:
  virtual void SetUp() override
  {
    HandlerMapTest::calls_to_my_positive_handler = 0;
    HandlerMapTest::calls_to_my_negative_handler = 0;
    HandlerMapTest::calls_to_my_validation_put_handler = 0;
  }

  virtual void TearDown() override
  {
  }

  static HandlerResult my_positive_handler(HandlerState& state)
  {
    calls_to_my_positive_handler++;

    return {};
  }

  static HandlerResult my_negative_handler(HandlerState& state)
  {
    calls_to_my_negative_handler++;

    return {"Negative handler"};
  }

  static HandlerResult my_validation_put_handler(HandlerState& state)
  {
    calls_to_my_validation_put_handler++;
    if (state.request().method() != web::http::methods::GET) {
      return {"Method not GET"};
    }

    return {};
  }

  static int calls_to_my_positive_handler;
  static int calls_to_my_negative_handler;
  static int calls_to_my_validation_put_handler;
};

int HandlerMapTest::calls_to_my_positive_handler {0};
int HandlerMapTest::calls_to_my_negative_handler {0};
int HandlerMapTest::calls_to_my_validation_put_handler {0};

TEST_F(HandlerMapTest, construct_using_default_ctor)
{
  HandlerMap tgt;
  handler_type h = tgt.handler({});
  EXPECT_FALSE(h);
}

TEST_F(HandlerMapTest, construct_using_handler_ctor)
{
  HandlerMap tgt {my_positive_handler};
  handler_type h = tgt.handler({});
  EXPECT_TRUE(h);
}

TEST_F(HandlerMapTest, construct_using_copy_ctor)
{
  HandlerMap rhs {};
  rhs.addHandler({"path", "first"}, my_positive_handler);
  rhs.addHandler({"path", "second"}, my_positive_handler);
  HandlerMap tgt(rhs);
  // target has copied resources
  handler_type h = tgt.handler({"path", "first"});
  EXPECT_TRUE(h);
  h = tgt.handler({"path", "second"});
  EXPECT_TRUE(h);
  // original still has its own resources
  h = rhs.handler({"path", "first"});
  EXPECT_TRUE(h);
  h = rhs.handler({"path", "second"});
  EXPECT_TRUE(h);
}

TEST_F(HandlerMapTest, construct_using_move_ctor)
{
  HandlerMap rhs {};
  rhs.addHandler({"path", "first"}, my_positive_handler);
  rhs.addHandler({"path", "second"}, my_positive_handler);
  HandlerMap tgt(std::move(rhs));
  // target has moved resources
  handler_type h = tgt.handler({"path", "first"});
  EXPECT_TRUE(h);
  h = tgt.handler({"path", "second"});
  EXPECT_TRUE(h);
  // original no longer has its own resources
  h = rhs.handler({"path", "first"});
  EXPECT_FALSE(h);
  h = rhs.handler({"path", "second"});
  EXPECT_FALSE(h);
}

TEST_F(HandlerMapTest, assign_with_assignment_copy)
{
  HandlerMap rhs {};
  rhs.addHandler({"dog", "alpha"}, my_positive_handler);
  rhs.addHandler({"dog", "bravo"}, my_positive_handler);
  HandlerMap tgt = rhs;
  // target has copied resources
  EXPECT_TRUE(tgt.handler({"dog", "alpha"}));
  EXPECT_TRUE(tgt.handler({"dog", "bravo"}));
  // original still has its own resources
  EXPECT_TRUE(rhs.handler({"dog", "alpha"}));
  EXPECT_TRUE(rhs.handler({"dog", "bravo"}));
}

TEST_F(HandlerMapTest, assign_with_assignment_move)
{
  HandlerMap rhs {};
  rhs.addHandler({"dog", "charlie"}, my_positive_handler);
  rhs.addHandler({"dog", "doug"}, my_positive_handler);
  HandlerMap tgt = std::move(rhs);
  // target has copied resources
  handler_type h = tgt.handler({"dog", "charlie"});
  EXPECT_TRUE(h);
  h = tgt.handler({"dog", "doug"});
  EXPECT_TRUE(h);
  // original no longer has its own resources
  h = rhs.handler({"dog", "charlie"});
  EXPECT_FALSE(h);
  h = rhs.handler({"dog", "doug"});
  EXPECT_FALSE(h);
}

TEST_F(HandlerMapTest, addHandler_adds_handlers)
{
  HandlerMap tgt;
  tgt.addHandler({"eat", "at", "chloes"}, my_positive_handler);
  tgt.addHandler({"kaise", "ho"}, my_negative_handler);
  handler_type h = tgt.handler({"eat", "at", "chloes"});
  EXPECT_TRUE(h);
  web::http::http_request req;
  service::ServiceMock svc;
  HandlerState state {req, svc};
  HandlerResult hr = h(state);
  EXPECT_TRUE(hr);
  h = tgt.handler({"kaise", "ho"});
  EXPECT_TRUE(h);
  hr = h(state);
  EXPECT_FALSE(hr);
  EXPECT_EQ(1, HandlerMapTest::calls_to_my_positive_handler);
  EXPECT_EQ(1, HandlerMapTest::calls_to_my_negative_handler);
}

}; // namespace cfx::controller

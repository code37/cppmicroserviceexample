/*******************************************************************************************************************************
 * test/controller/microservice_controller_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "controller/microservice_controller.h"
#include "controller/set_endpoint_exception.h"
#include "service/service_mock.h"

namespace cfx::controller {

class MicroserviceControllerTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(MicroserviceControllerTest, setEndpoint_success)
{
  service::ServiceMock svc;
  MicroserviceController target {svc};
  EXPECT_NO_THROW(target.setEndpoint(cfx::utils::HostFamily::Ip4, 6701, "/path/to/api"));
}

TEST_F(MicroserviceControllerTest, setEndpoint_invalid_path_failure)
{
  service::ServiceMock svc;
  MicroserviceController target {svc};
  EXPECT_THROW(target.setEndpoint(cfx::utils::HostFamily::Ip4, 8807, "!@#$%^(("), SetEndpointException);
}

} // namespace cfx::controller

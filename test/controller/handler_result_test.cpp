/*******************************************************************************************************************************
 * test/controller/handler_result_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "controller/handler_result.h"

namespace cfx::controller {

class HandlerResultTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(HandlerResultTest, construct_default_is_handled)
{
  HandlerResult tgt;
  EXPECT_TRUE(tgt.handled());
  EXPECT_TRUE(tgt.reason().empty());
  EXPECT_TRUE(tgt);
}

TEST_F(HandlerResultTest, construct_reason_is_failed)
{
  HandlerResult tgt {"He's dead Jim."};
  EXPECT_FALSE(tgt.handled());
  EXPECT_EQ("He's dead Jim.", tgt.reason());
  EXPECT_FALSE(tgt);
}

TEST_F(HandlerResultTest, construct_throws_on_empty_reason)
{
  EXPECT_THROW(HandlerResult tgt {""}, cfx::utils::UsageException);
}

TEST_F(HandlerResultTest, copy_ctor_copies_values)
{
  HandlerResult seed {"1237"};
  HandlerResult tgt {seed};
  EXPECT_EQ(seed.handled(), tgt.handled());
  EXPECT_EQ(seed.reason(), tgt.reason());
}

TEST_F(HandlerResultTest, copy_ctor_moves_values)
{
  HandlerResult seed {"abcdg"};
  HandlerResult tgt {std::move(seed)};
  EXPECT_FALSE(tgt.handled());
  EXPECT_EQ("abcdg", tgt.reason());
  EXPECT_TRUE(seed.reason().empty());
}

TEST_F(HandlerResultTest, copy_assignment_copies_values)
{
  HandlerResult seed {"1237"};
  HandlerResult tgt = seed;
  EXPECT_EQ(seed.handled(), tgt.handled());
  EXPECT_EQ(seed.reason(), tgt.reason());
}

TEST_F(HandlerResultTest, copy_assignment_moves_values)
{
  HandlerResult seed {"abcdg"};
  HandlerResult tgt = std::move(seed);
  EXPECT_FALSE(tgt.handled());
  EXPECT_EQ("abcdg", tgt.reason());
  EXPECT_TRUE(seed.reason().empty());
}

}; // namespace cfx::controller

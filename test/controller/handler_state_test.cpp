/*******************************************************************************************************************************
 * test/controller/handler_state_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <cpprest/http_msg.h>

#include "controller/handler_state.h"
#include "service/service_mock.h"

namespace cfx::controller {

class HandlerStateTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(HandlerStateTest, construct_with_request_and_service_is_handled)
{
  web::http::http_request req;
  service::ServiceMock svc;
  EXPECT_NO_THROW({
    HandlerState state(req, svc);
  });
}

TEST_F(HandlerStateTest, constructor_initializes_members)
{
  web::http::http_request req;
  service::ServiceMock svc;
  HandlerState state(req, svc);
  EXPECT_EQ(&req, &state.request());
  EXPECT_EQ(&svc, &state.service());
}

TEST_F(HandlerStateTest, copy_ctor_copies_values)
{
  web::http::http_request req;
  service::ServiceMock svc;
  HandlerState seed {req, svc};
  HandlerState tgt {seed};
  EXPECT_EQ(&seed.request(), &tgt.request());
  EXPECT_EQ(&seed.service(), &tgt.service());
}

TEST_F(HandlerStateTest, copy_assignment_copies_values)
{
  web::http::http_request req;
  service::ServiceMock svc;
  HandlerState seed {req, svc};
  HandlerState tgt = seed;
  EXPECT_EQ(&seed.request(), &tgt.request());
  EXPECT_EQ(&seed.service(), &tgt.service());
}

}; // namespace cfx::controller

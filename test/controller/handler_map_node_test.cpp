/*******************************************************************************************************************************
 * test/controller/handler_map_node_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <cpprest/http_msg.h>

#include "controller/handler_map_node.h"
#include "controller/handler_state.h"
#include "controller/no_handler_exception.h"
#include "service/service_mock.h"

namespace cfx::controller {

class HandlerMapNodeTest : public ::testing::Test
{
protected:
  virtual void SetUp() override
  {
    HandlerMapNodeTest::calls_to_my_handler = 0;
  }

  virtual void TearDown() override
  {
  }

  static HandlerResult my_handler(HandlerState& state)
  {
    calls_to_my_handler++;

    return {};
  }

  static int calls_to_my_handler;
};

int HandlerMapNodeTest::calls_to_my_handler {0};

TEST_F(HandlerMapNodeTest, construct_using_default_ctor)
{
  HandlerMapNode tgt;
  EXPECT_TRUE(tgt.key().empty());
  EXPECT_EQ(size_t(0), tgt.nodes().size());
  EXPECT_FALSE(tgt.hasHandler());
}

TEST_F(HandlerMapNodeTest, construct_using_key_ctor)
{
  HandlerMapNode tgt("key_zee");
  EXPECT_EQ("key_zee", tgt.key());
  EXPECT_EQ(size_t(0), tgt.nodes().size());
  EXPECT_FALSE(tgt.hasHandler());
}

TEST_F(HandlerMapNodeTest, construct_using_key_handler_ctor)
{
  HandlerMapNode tgt("key_zippee", my_handler);
  EXPECT_EQ("key_zippee", tgt.key());
  EXPECT_EQ(size_t(0), tgt.nodes().size());
  EXPECT_TRUE(tgt.hasHandler());
}

TEST_F(HandlerMapNodeTest, construct_using_copy_ctor)
{
  HandlerMapNode base("456", my_handler);
  base.addNode({"subnode_a", my_handler});
  base.addNode({"subnode_b"});
  HandlerMapNode tgt(base);

  EXPECT_EQ("456", tgt.key());
  EXPECT_EQ(size_t(2), tgt.nodes().size());
  EXPECT_TRUE(tgt.hasHandler());

  HandlerMapNode* psa = tgt.findNode("subnode_a");
  EXPECT_NE(nullptr, psa);
  EXPECT_EQ("subnode_a", psa->key());
  EXPECT_TRUE(psa->hasHandler());

  HandlerMapNode* psb = tgt.findNode("subnode_b");
  EXPECT_NE(nullptr, psb);
  EXPECT_EQ("subnode_b", psb->key());
  EXPECT_FALSE(psb->hasHandler());
}

TEST_F(HandlerMapNodeTest, construct_using_move_ctor)
{
  HandlerMapNode base("twenty-one", my_handler);
  base.addNode({"alphanode", my_handler});
  base.addNode({"bravonode"});
  HandlerMapNode tgt(std::move(base));

  EXPECT_EQ("twenty-one", tgt.key());
  EXPECT_EQ(size_t(2), tgt.nodes().size());
  EXPECT_TRUE(tgt.hasHandler());

  HandlerMapNode* psa = tgt.findNode("alphanode");
  EXPECT_NE(nullptr, psa);
  EXPECT_EQ("alphanode", psa->key());
  EXPECT_TRUE(psa->hasHandler());

  HandlerMapNode* psb = tgt.findNode("bravonode");
  EXPECT_NE(nullptr, psb);
  EXPECT_EQ("bravonode", psb->key());
  EXPECT_FALSE(psb->hasHandler());

  EXPECT_TRUE(base.key().empty());
  EXPECT_EQ(size_t(0), base.nodes().size());
  EXPECT_FALSE(base.hasHandler());
}

TEST_F(HandlerMapNodeTest, assign_with_assignment_copy)
{
  HandlerMapNode tgt {"murvin"};
  HandlerMapNode rhs("reese", my_handler);
  rhs.addNode({"a->node", my_handler});
  tgt = rhs;

  EXPECT_EQ("reese", tgt.key());
  EXPECT_EQ(size_t(1), tgt.nodes().size());
  EXPECT_TRUE(tgt.hasHandler());

  HandlerMapNode* psa = tgt.findNode("a->node");
  EXPECT_NE(nullptr, psa);
  EXPECT_EQ("a->node", psa->key());
  EXPECT_TRUE(psa->hasHandler());

  EXPECT_EQ("reese", rhs.key());
  EXPECT_EQ(size_t(1), rhs.nodes().size());
  EXPECT_TRUE(rhs.hasHandler());
}

TEST_F(HandlerMapNodeTest, assign_with_assignment_move)
{
  HandlerMapNode tgt {"moovin"};
  HandlerMapNode rhs("Reeze", my_handler);
  rhs.addNode({"Zener", my_handler});
  tgt = std::move(rhs);

  EXPECT_EQ("Reeze", tgt.key());
  EXPECT_EQ(size_t(1), tgt.nodes().size());
  EXPECT_TRUE(tgt.hasHandler());

  HandlerMapNode* psa = tgt.findNode("Zener");
  EXPECT_NE(nullptr, psa);
  EXPECT_EQ("Zener", psa->key());
  EXPECT_TRUE(psa->hasHandler());

  EXPECT_TRUE(rhs.key().empty());
  EXPECT_EQ(size_t(0), rhs.nodes().size());
  EXPECT_FALSE(rhs.hasHandler());
}

TEST_F(HandlerMapNodeTest, handler_set_and_get)
{
  HandlerMapNode tgt {"geanie"};
  EXPECT_FALSE(tgt.hasHandler());

  tgt.setHandler(my_handler);
  EXPECT_TRUE(tgt.hasHandler());

  handler_type h = tgt.handler();
  web::http::http_request req;
  service::ServiceMock svc;
  HandlerState state(req, svc);

  HandlerResult hres = h(state);
  EXPECT_TRUE(hres);
  EXPECT_EQ(1, HandlerMapNodeTest::calls_to_my_handler);

  tgt.setHandler(nullptr);
  EXPECT_FALSE(tgt.hasHandler());
}

TEST_F(HandlerMapNodeTest, addNode_by_copy_creates_nodes_with_copied_resources)
{
  HandlerMapNode tgt("key_dee");
  HandlerMapNode anode("anode", my_handler);
  HandlerMapNode cathode("cathode");
  tgt.addNode(anode);
  tgt.addNode(cathode);

  HandlerMapNode* phn {};
  phn = tgt.findNode("cathode");
  EXPECT_NE(nullptr, phn);
  EXPECT_NE(&cathode, phn);
  EXPECT_EQ(cathode.key(), phn->key());
  EXPECT_FALSE(phn->hasHandler());
  phn = tgt.findNode("anode");
  EXPECT_NE(nullptr, phn);
  EXPECT_NE(&anode, phn);
  EXPECT_EQ(anode.key(), phn->key());
  EXPECT_TRUE(phn->hasHandler());
}

TEST_F(HandlerMapNodeTest, addNode_by_move_creates_nodes_with_moved_resources)
{
  HandlerMapNode tgt("key_x");
  HandlerMapNode subnode("subnode88", my_handler);
  subnode.addNode({"subsubnode"});
  tgt.addNode(std::move(subnode));

  HandlerMapNode* phn {};
  phn = tgt.findNode("subnode88");
  EXPECT_NE(nullptr, phn);
  EXPECT_NE(&subnode, phn);
  EXPECT_EQ("subnode88", phn->key());
  EXPECT_TRUE(subnode.key().empty());
  EXPECT_EQ(size_t(1), phn->nodes().size());
  EXPECT_EQ(size_t(0), subnode.nodes().size());
  EXPECT_TRUE(phn->hasHandler());
  EXPECT_FALSE(subnode.hasHandler());
}

TEST_F(HandlerMapNodeTest, relational_eq)
{
  HandlerMapNode a("AbCd");
  HandlerMapNode b("Ebnd");
  HandlerMapNode c("AbCd");

  ASSERT_TRUE(a == c);
  ASSERT_FALSE(a == b);
  ASSERT_TRUE(a == "AbCd");
  ASSERT_FALSE(a == "AbCe");
  ASSERT_TRUE("AbCd" == a);
  ASSERT_FALSE("AbCf" == a);
}

TEST_F(HandlerMapNodeTest, relational_ne)
{
  HandlerMapNode a("AbCd");
  HandlerMapNode b("Ebnd");
  HandlerMapNode c("AbCd");

  ASSERT_TRUE(a != b);
  ASSERT_FALSE(a != c);
  ASSERT_TRUE(a != "AbCe");
  ASSERT_FALSE(a != "AbCd");
  ASSERT_TRUE("AbCf" != a);
  ASSERT_FALSE("AbCd" != a);
}

TEST_F(HandlerMapNodeTest, relational_lt)
{
  HandlerMapNode a("aaa");
  HandlerMapNode b("bbb");
  HandlerMapNode c("ccc");

  ASSERT_TRUE(b < c);
  ASSERT_FALSE(b < a);
  ASSERT_TRUE(b < "ccc");
  ASSERT_FALSE(b < "aaa");
  ASSERT_TRUE("aaa" < b);
  ASSERT_FALSE("ccc" < b);
}

TEST_F(HandlerMapNodeTest, relational_gt)
{
  HandlerMapNode a("aaa");
  HandlerMapNode b("bbb");
  HandlerMapNode c("ccc");

  ASSERT_TRUE(b > a);
  ASSERT_FALSE(b > c);
  ASSERT_TRUE(b > "aaa");
  ASSERT_FALSE(b > "ccc");
  ASSERT_TRUE("ccc" > b);
  ASSERT_FALSE("aaa" > b);
}

TEST_F(HandlerMapNodeTest, relational_lte)
{
  HandlerMapNode a("aaa");
  HandlerMapNode b("bbb");
  HandlerMapNode b2("bbb");
  HandlerMapNode c("ccc");

  ASSERT_TRUE(b <= c);
  ASSERT_TRUE(b <= b2);
  ASSERT_FALSE(b <= a);
  ASSERT_TRUE(b <= "ccc");
  ASSERT_TRUE(b <= "bbb");
  ASSERT_FALSE(b <= "aaa");
  ASSERT_TRUE("aaa" <= b);
  ASSERT_TRUE("bbb" <= b);
  ASSERT_FALSE("ccc" <= b);
}

TEST_F(HandlerMapNodeTest, relational_gte)
{
  HandlerMapNode a("aaa");
  HandlerMapNode b("bbb");
  HandlerMapNode b2("bbb");
  HandlerMapNode c("ccc");

  ASSERT_TRUE(c >= b);
  ASSERT_TRUE(b2 >= b);
  ASSERT_FALSE(a >= b);
  ASSERT_TRUE(b >= "aaa");
  ASSERT_TRUE(b >= "bbb");
  ASSERT_FALSE(b >= "ccc");
  ASSERT_TRUE("ccc" >= b);
  ASSERT_TRUE("bbb" >= b);
  ASSERT_FALSE("aaa" >= b);
}

}; // namespace cfx::controller

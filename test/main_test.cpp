/*******************************************************************************************************************************
 * test/main_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "utils/logging.h"

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);

  cfx::utils::Logging::init(cfx::utils::LogFormat::Debug, cfx::utils::LogLevel::Debug);

  return RUN_ALL_TESTS();
}

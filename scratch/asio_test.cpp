/*******************************************************************************************************************************
 * utils/asio_test.cpp
 ******************************************************************************************************************************/

#include "gtest/gtest.h"
#include <boost/asio.hpp>

// TODO: Remove when no longer needed
#include <iostream>

namespace cfx::utils {

class AsioTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(AsioTest, hostName)
{
  std::string hn {boost::asio::ip::host_name()};
  std::cout << "Host name: " << hn << std::endl;
}

TEST_F(AsioTest, hostIp4)
{
  boost::asio::io_context iox;
  boost::asio::ip::tcp::resolver rv(iox);
  boost::asio::ip::tcp::resolver::query qry(boost::asio::ip::host_name(), "");

  std::cout << "Query host name: " << qry.host_name() << std::endl;
  std::cout << "Query service name: " << qry.service_name() << std::endl;

  auto info {rv.resolve(qry)};
  boost::asio::ip::tcp::resolver::iterator iend;
  while (info != iend) {
    boost::asio::ip::tcp::endpoint ep = *info++;
    sockaddr sa = *ep.data();

    std::cout << "sa.family: " << sa.sa_family << std::endl;
    std::cout << "ep.port: " << ep.port() << std::endl;
    //std::cout << "ep.protocol: " << ep.protocol().to_string() << std::endl;
    std::cout << "ep.address: " << ep.address().to_string() << std::endl;

  }
}

} // namespace cfx::utils

/*******************************************************************************************************************************
 * controller/controller_interface.h
 ******************************************************************************************************************************/

#ifndef CFX_CONTROLLER_CONTROLLER_INTERFACE_H
#define CFX_CONTROLLER_CONTROLLER_INTERFACE_H

#include <cpprest/http_msg.h>

namespace cfx::controller {

class ControllerInterface
{
public:
  virtual void handleGet(web::http::http_request) = 0;
  virtual void handlePut(web::http::http_request) = 0;

protected:
  ControllerInterface() {}
  virtual ~ControllerInterface() {}
};

} // namespace cfx::controller

#endif // CFX_CONTROLLER_CONTROLLER_INTERFACE_H

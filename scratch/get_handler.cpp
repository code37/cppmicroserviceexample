/*******************************************************************************************************************************
 * handlers/get_handler.cpp
 ******************************************************************************************************************************/

#include "constants/constants.h"
#include "constants/version.h"
#include "handlers/get_handler.h"

namespace cfx::handlers {

GetHandler::GetHandler()
{
}

GetHandler::~GetHandler()
{
}

void GetHandler::handle(web::http::http_request req)
{
  bool handled = false;
  std::vector<std::string> path = requestPath(req);
  if (!path.empty()) {
    if (path.size() > 1 && path[0] == "service" && path[1] == "test") {
      handled = handleServiceTest(req, path);
    }
  }

  if (!handled) {
    auto response = web::json::value::object();
    response["service_name"] = web::json::value::string(cfx::constants::ProjectName);
    response["http_method"] = web::json::value::string(req.method());
    response["path"] = web::json::value::string(web::uri::decode(req.relative_uri().path()));
    req.reply(web::http::status_codes::NotFound, response);
  }
}

bool GetHandler::handleServiceTest(web::http::http_request req, std::vector<std::string>& path)
{
  if (path.size() > 2) {

    return false;
  }

  auto response = web::json::value::object();
  response["service_name"] = web::json::value::string(cfx::constants::ProjectName);
  response["timestamp"] = web::json::value::string(cfx::constants::ProjectBuildTimestamp);
  response["commit"] = web::json::value::string(cfx::constants::ProjectCommitHash);
  response["version"] = web::json::value::string(cfx::constants::ProjectSemanticVersion);
  response["status"] = web::json::value::string("ready!");
  req.reply(web::http::status_codes::OK, response);

  return true;
}

} // namespace cfx::handlers

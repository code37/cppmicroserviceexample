#!/bin/sh
echo "Cleaning bin/ and obj/ directories"
rm --recursive --force --verbose bin/* obj/*
